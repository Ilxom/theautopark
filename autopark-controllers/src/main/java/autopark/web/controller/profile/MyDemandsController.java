package autopark.web.controller.profile;

import autopark.domain.Role;
import autopark.dto.DemandDTO;
import autopark.service.IDemandService;
import autopark.web.auth.RequiresAuthentication;
import autopark.web.controller.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by aro on 07.03.2016.
 */
@Controller
public class MyDemandsController {
    private final static Logger logger = LoggerFactory.getLogger(MyDemandsController.class);

    @Autowired
    private IDemandService demandService;


    @RequestMapping(value = "/mydemands")
    @RequiresAuthentication({Role.ROLE_USER})
    public String handle(ModelMap modelMap) {
        logger.debug("mydemands");

        List<DemandDTO> list = demandService.getMyDemands();

        modelMap.put(Constants.LIST,list);

        return "/WEB-INF/content/profile/mydemands.jsp";
    }


}
