package autopark.web.controller;

import autopark.dto.CarDTO;
import autopark.service.ICarService;
import autopark.service.IWebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Controller
public class MainPageController {

    @Autowired
    @Qualifier("carServiceHibernate")
    private ICarService carServiceHibernate;

    @Autowired
    @Qualifier("carServiceJdbc")
    private ICarService carServiceJdbc;

    @Autowired
    private IWebService webService;


    @RequestMapping(value = {"/main","/"})
    public String handle(ModelMap modelMap) {
        return handleRequest(modelMap);
    }


    private String handleRequest(ModelMap modelMap){


        ExecutorService executor = Executors.newFixedThreadPool(2);

        List<Callable<ThreadResult>> callables = Arrays.asList(
            new Callable<ThreadResult>() {
                @Override
                public ThreadResult call() throws Exception {
                    ThreadResult result = new ThreadResult();
                    result.thread = "Hibernate";
                    result.list = carServiceHibernate.getCars();
                    return result;
                }
            },
            new Callable<ThreadResult>() {
                @Override
                public ThreadResult call() throws Exception {
                    ThreadResult result = new ThreadResult();
                    result.thread = "JDBC";
                    result.list = carServiceJdbc.getCars();
                    return result;
                }
            }



        );

        ThreadResult res = null;
        try {
            res = executor.invokeAny(callables);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        executor.shutdown();

        modelMap.put("cars",res.list);
        modelMap.put("thread",res.thread);
        modelMap.put("author",System.getProperty("user.name"));


        return "/WEB-INF/content/main.jsp";
    }


    private class ThreadResult{
        String thread;
        List<CarDTO> list;
    }



}



