package autopark.web.controller.image;

import autopark.dao.ImageFileDAO;
import autopark.domain.ImageFile;
import autopark.dto.PrincipalUser;
import autopark.web.controller.Constants;
import autopark.web.utils.SecurityUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by aro on 09.04.2016.
 */
@Controller
public class ImageFileController {
    private static final Log log = LogFactory.getLog(ImageFileController.class);

    @Autowired
    private ImageFileDAO imageFileDAO;

    @RequestMapping(value = "/imf/{id}")
    public String get(@PathVariable Long id,@RequestParam(required = false) String suffix, HttpServletRequest request, HttpServletResponse response) {
        PrincipalUser principalUser = SecurityUtils.getPrincipalUser();

        /*
        if(principalUser == null){
            return null;
        }
        */

        return process(id,suffix,request,response);
    }

    private String process(Long id,String suffix,HttpServletRequest request, HttpServletResponse response){
        InputStream imgStream = null;

        ImageFile tempFile = null;
        try {
            tempFile = imageFileDAO.get(id);
            String fileName = tempFile.getPathfolder() +id  + Constants.PATH_DELIMITER + "tf.f";
            if(suffix != null){
                fileName+=suffix;
            }
            java.io.File tf = new java.io.File(fileName);
            if(tf != null && tf.exists()){
                imgStream = new FileInputStream(tf);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }

        if(imgStream == null){
            return null;
        }

        ServletOutputStream sos = null;
        try {
            String enc = "utf-8";
            String userAgent = request.getHeader("user-agent");
            boolean isInternetExplorer = (userAgent.indexOf("MSIE") > -1);

            if(tempFile != null){
                String fileName = tempFile.getFileName();
                byte[] fileNameBytes = fileName.getBytes((isInternetExplorer) ? ("windows-1250") : ("utf-8"));
                String dispositionFileName = "";
                for (byte b: fileNameBytes) dispositionFileName += (char)(b & 0xff);

                response.setCharacterEncoding(enc);
                response.setHeader("content-disposition", "filename=\""+ dispositionFileName+"\"");
            }
            sos = response.getOutputStream();

            IOUtils.copy(imgStream, sos);
            sos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                sos.close();
                imgStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;

    }

}

