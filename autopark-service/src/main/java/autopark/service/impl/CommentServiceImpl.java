package autopark.service.impl;

import autopark.dao.ICommentDAO;
import autopark.domain.Comment;
import autopark.domain.CommentEntityEnum;
import autopark.dto.CommentDTO;
import autopark.service.ICommentService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * 21.03.2016.
 */

@Service
public class CommentServiceImpl implements ICommentService {
    private final Logger log = LoggerFactory.getLogger(CommentServiceImpl.class);

    @Autowired
    private ICommentDAO commentDAO;

    @Override
    public Comment createCarComment(CommentDTO dto) {
        if(dto!=null && StringUtils.isNotEmpty(dto.getDescription()) && StringUtils.isNotEmpty(dto.getCommentatorName())){
            Comment comment = new Comment();
            comment.setCreationDate(new Date());
            comment.setDescription(dto.getDescription());
            comment.setCommentatorName(dto.getCommentatorName());
            comment.setEntity(CommentEntityEnum.car);
            comment.setEntityId(dto.getEntityId());
            commentDAO.save(comment);
            return comment;
        }

        return null;
    }
}
