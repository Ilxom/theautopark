package autopark.service.impl;

import autopark.dao.IRoleUserDAO;
import autopark.dao.IUserDAO;
import autopark.domain.ImageFile;
import autopark.domain.Role;
import autopark.domain.RoleUser;
import autopark.domain.User;
import autopark.dto.ImageFileDTO;
import autopark.dto.PrincipalUser;
import autopark.dto.RoleDTO;
import autopark.dto.UserDTO;
import autopark.service.AutoparkServiceException;
import autopark.service.IEmailSender;
import autopark.service.IUserService;
import autopark.service.ImageService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

@Service("myUserService")
public class UserServiceImpl implements IUserService, UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private IUserDAO userDAO;


    @Autowired
    @Qualifier("baseEmailSender")
    private IEmailSender emailSender;

    @Autowired
    @Qualifier("encoder")
    private PasswordEncoder passwordEncoder;

    @Autowired
    ImageService imageService;

    @Autowired
    IRoleUserDAO roleUserDAO;

    @Transactional
    public boolean createUserV1(final UserDTO dto, final String baseURL) {
        User user = userDAO.getByEmail(dto.getEmail());

        if (user != null) {
            return false;
        }
        user = DTOAssembler.assembleUser(dto);
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setCreationDate(new Date());

        if (dto.getImageFileDTO() != null) {
            try {
                ImageFileDTO tfd = dto.getImageFileDTO();
                ImageFile imageFile = imageService.uploadFile(tfd);
                if (imageFile != null) {
                    user.setMyPhotoImageFileId(imageFile.getId());
                }
            } catch (AutoparkServiceException e) {
                log.error("cannot store image", e);
            }
        }

        final Long userId = userDAO.save(user);


        //create role, thread
        RoleUser roleUser = new RoleUser();
        roleUser.setUSER_ID(userId);
        roleUser.setROLE_ID(Role.ROLE_USER_ID);
        roleUserDAO.save(roleUser);


        //send email
        emailSender.sendCreationEmail(baseURL, dto);

        user.setCreationDate(new Date());
        userDAO.merge(user);
        return true;
    }


    @Transactional
    public boolean createUserV2(final UserDTO dto, final String baseURL) {
        User user = userDAO.getByEmail(dto.getEmail());

        if (user != null) {
            return false;
        }
        user = DTOAssembler.assembleUser(dto);
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setCreationDate(new Date());

        if (dto.getImageFileDTO() != null) {
            try {
                ImageFileDTO tfd = dto.getImageFileDTO();
                ImageFile imageFile = imageService.uploadFile(tfd);
                if (imageFile != null) {
                    user.setMyPhotoImageFileId(imageFile.getId());
                }
            } catch (AutoparkServiceException e) {
                log.error("cannot store image", e);
            }
        }

        final Long userId = userDAO.save(user);


        //create role, thread
        Thread roleThread = new Thread(new Runnable() {
            public void run() {//Этот метод будет выполняться в побочном потоке
                RoleUser roleUser = new RoleUser();
                roleUser.setUSER_ID(userId);
                roleUser.setROLE_ID(Role.ROLE_USER_ID);
                roleUserDAO.save(roleUser);
            }
        });
        roleThread.start();


        //send email
        new Thread() {
            public void run() {
                emailSender.sendCreationEmail(baseURL, dto);
            }
        }.start();

        user.setCreationDate(new Date());
        userDAO.merge(user);
        return true;
    }


    @Transactional
    public boolean createUserV3(final UserDTO dto, final String baseURL) {
        ExecutorService executor = Executors.newFixedThreadPool(2);
        User user = userDAO.getByEmail(dto.getEmail());

        if (user != null) {
            return false;
        }
        user = DTOAssembler.assembleUser(dto);
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setCreationDate(new Date());

        if (dto.getImageFileDTO() != null) {
            try {
                ImageFileDTO tfd = dto.getImageFileDTO();
                ImageFile imageFile = imageService.uploadFile(tfd);
                if (imageFile != null) {
                    user.setMyPhotoImageFileId(imageFile.getId());
                }
            } catch (AutoparkServiceException e) {
                log.error("cannot store image", e);
            }
        }


        final Long userId = userDAO.save(user);


        //create role, thread
        executor.execute(new Runnable() {
            public void run() {//Этот метод будет выполняться в побочном потоке
                RoleUser roleUser = new RoleUser();
                roleUser.setUSER_ID(userId);
                roleUser.setROLE_ID(Role.ROLE_USER_ID);
                roleUserDAO.save(roleUser);
            }
        });


        //send email
        executor.execute(new Runnable() {
            public void run() {
                emailSender.sendCreationEmail(baseURL, dto);
            }
        });


        try {
            log.info("attempt to shutdown executor");
            executor.shutdown();
            executor.awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            log.info("tasks interrupted");
        } finally {
            if (!executor.isTerminated()) {
                log.warn("cancel non-finished tasks");
            }
            executor.shutdownNow();
            log.info("shutdown finished");
        }


        user.setCreationDate(new Date());
        userDAO.merge(user);
        return true;
    }



    @Transactional
    public boolean createUserV4(final UserDTO dto, final String baseURL) {
        ExecutorService executor = Executors.newFixedThreadPool(2);
        User user = userDAO.getByEmail(dto.getEmail());

        if (user != null) {
            return false;
        }

        Future<User> future = executor.submit(new Callable<User>() {
            @Override
            public User call() throws Exception {
                User user = DTOAssembler.assembleUser(dto);
                user.setPassword(passwordEncoder.encode(dto.getPassword()));
                user.setCreationDate(new Date());

                if (dto.getImageFileDTO() != null) {
                    try {
                        ImageFileDTO tfd = dto.getImageFileDTO();
                        ImageFile imageFile = imageService.uploadFile(tfd);
                        if (imageFile != null) {
                            user.setMyPhotoImageFileId(imageFile.getId());
                        }
                    } catch (AutoparkServiceException e) {
                        log.error("cannot store image", e);
                    }
                }
                return user;
            }
        });
        try {
            user = future.get(3, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }


        final Long userId = userDAO.save(user);



        List<Callable<String>> callables = Arrays.asList(
                //create role, thread
                new Callable<String>() {
                    @Override
                    public String call() throws Exception {
                        RoleUser roleUser = new RoleUser();
                        roleUser.setUSER_ID(userId);
                        roleUser.setROLE_ID(Role.ROLE_USER_ID);
                        return roleUserDAO.save(roleUser).toString();
                    }
                },
                new Callable<String>() {
                    //send email
                    @Override
                    public String call() throws Exception {
                        emailSender.sendCreationEmail(baseURL, dto);
                        return "ok";
                    }
                });

        try {
            List<Future<String>> futures = executor.invokeAll(callables);
            for(Future<String> stringFuture : futures){
                String s = stringFuture.get(3,TimeUnit.SECONDS);
                System.out.println(s);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }

        try {
            log.info("attempt to shutdown executor");
            executor.shutdown();
            executor.awaitTermination(5, TimeUnit.SECONDS);
        }
        catch (InterruptedException e) {
            log.info("tasks interrupted");
        }
        finally {
            if (!executor.isTerminated()) {
                log.warn("cancel non-finished tasks");
            }
            executor.shutdownNow();
            log.info("shutdown finished");
        }


        user.setCreationDate(new Date());
        userDAO.merge(user);
        return true;
    }


    @Transactional
    public boolean createUserV5(final UserDTO dto, final String baseURL) {
        return true;
    }




    @Override
    public PrincipalUser loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userDAO.getByEmail(s);
        return assemblePrincipalUser(user, false);
    }

    public UserDTO getUserByEmail(String email) {
        User user = userDAO.getByEmail(email);
        if (user == null) {
            return null;
        }
        return DTOAssembler.assembleUserDTO(user);
    }


    public boolean processLostPassword(String ourServer, String email) {
        return false;
    }


    @Override
    public boolean updateUserPassword(UserDTO userDTO, String baseURL) {
        User user = userDAO.getByEmail(userDTO.getEmail());
        if (user == null) {
            return false;
        }
        user.setPassword(userDTO.getPassword());
        /*userDTO.getPassword();*/
        userDAO.update(user);

        return true;

    }


    public PrincipalUser assemblePrincipalUser(User user, Boolean facebook) {
        if (user != null) {
            PrincipalUser springUser = new PrincipalUser();
            springUser.setEnabled(true);
            springUser.setPassword(user.getPassword());
            springUser.setUsername(StringUtils.isNotEmpty(user.getEmail()) ? user.getEmail() : user.getFacebookId());
            springUser.setFacebook(facebook);
            if (user.getAuthorities() != null) {
                List<RoleDTO> roles = new ArrayList<RoleDTO>();
                for (Role role : user.getAuthorities()) {
                    RoleDTO dto = new RoleDTO();
                    dto.setAuthority(role.getAuthority());
                    roles.add(dto);
                }
                springUser.setAuthorities(roles);
            }
            return springUser;
        }
        return null;
    }

    public UserDTO getUser() {
        //сделать здесь получение юзера
        PrincipalUser springUser = (PrincipalUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = null;
        if (springUser.getFacebook() != null && springUser.getFacebook()) {
            user = userDAO.getByFacebookId(springUser.getUsername());
        }
        if (user == null) {
            user = userDAO.getByEmail(springUser.getUsername());
        }

        if (user != null) {
            return DTOAssembler.assembleUserDTO(user);
        }
        return null;
    }

    public User getCurrentUser() throws AutoparkServiceException {
        User user = null;
        try {
            PrincipalUser springUser = (PrincipalUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            user = userDAO.getByEmail(springUser.getUsername());
            if (user == null) {
                user = userDAO.getByFacebookId(springUser.getUsername());
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new AutoparkServiceException("Not authenticated user");
        }
        return user;
    }

    public List<UserDTO> getTestUsers(String phone) {
        List<User> users = userDAO.getByPhone(phone);
        List<UserDTO> res = new ArrayList<UserDTO>();
        if (users != null) {

            for (User user : users) {
                res.add(DTOAssembler.assembleUserDTO(user));
            }
        }
        return res;
    }

    public void removeTestUsers(String phone) {
        roleUserDAO.removeRolesOfTestUsers(phone);

        userDAO.removeTestUsers(phone);

    }
}