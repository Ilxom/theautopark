<%--http://www.w3schools.com/bootstrap/bootstrap_tables.asp--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tags" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <%@ include file="../tiles/head.jsp" %>

        <link rel="stylesheet" type="text/css" href="/styles/mydemands.css" />
    </head>

    <body>
        <tags:headerV2 activeProfile="active"/>

        <div id="wrap">

        <div class="container">

            <tags:profilebar activeDemands="active"/>

            <div>
                <div>
                    <a href="#" class="btn btn-success btn-lg">
                        <span class="glyphicon glyphicon-plus"></span>Новая заявка
                    </a>

                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID заявки</th>
                            <th>Тип машины</th>
                            <th><fmt:message key="loading.capacity"/></th>
                            <th>Описание заявки</th>
                            <th>Оплата,руб</th>
                            <th>Дата работы</th>
                            <th>Дата создания</th>
                            <th>Просмотр</th>
                        </tr>
                        </thead>
                        <c:if test="${list != null}">
                            <tbody>
                            <c:forEach items="${list}" var="item"  varStatus="s">
                                <tr class="${rowClass}">
                                    <td>
                                        <label>
                                            <a href="/mydemand/${item.id}">${item.id}</a>
                                        </label>
                                    </td>
                                    <td>
                                        <label><fmt:message key="${item.type}.displayName"/></label>
                                    </td>
                                    <td>
                                        <label><fmt:message key="${item.capacity}.displayName"/></label>
                                    </td>
                                    <td>
                                        <label>${item.description}</label>
                                    </td>
                                    <td>
                                        <label>${item.settlement}</label>
                                    </td>
                                    <td>
                                        <label><fmt:formatDate value="${item.startWorkDate}" pattern="dd.MM.yyyy" /></label>
                                    </td>
                                    <td>
                                        <label><fmt:formatDate value="${item.creationDate}" pattern="dd.MM.yyyy" /></label>
                                    </td>

                                    <td>
                                        <a href="/car/${item.id}"><img width="16" height="16" src="/img/arrow1.png"></a>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </c:if>
                    </table>


                </div>

            </div>

        </div>
        </div>



        <hr>
        <%@ include file="../tiles/footer.jsp" %>


    </body>
</html>