<%@tag pageEncoding="UTF-8" %>
<%@ attribute name="activeCars" type="java.lang.String" required="false" %>
<%@ attribute name="activeDemands" type="java.lang.String" required="false" %>
<%@ attribute name="activeAnn" type="java.lang.String" required="false" %>
<%@ attribute name="activeProfile" type="java.lang.String" required="false" %>


<br/>
<span class="title2">Личный кабинет</span>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <li class="${activeProfile}"><a href="/profile">Профиль</a></li>
            <li class="${activeCars}"><a href="/mycars">Мои автомобили</a></li>
            <li class="${activeDemands}"><a href="/mydemands">Мои заявки</a></li>
            <li class="${activeAnn}"><a href="/myanns">Мои объявления</a></li>
        </ul>
    </div>
</nav>