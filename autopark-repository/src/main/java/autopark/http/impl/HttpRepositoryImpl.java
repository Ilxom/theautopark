package autopark.http.impl;

import autopark.AutoparkRepositoryException;
import autopark.http.IHttpRepository;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by aro on 27.03.2016.
 */
@Component
public class HttpRepositoryImpl implements IHttpRepository{
    private static final Logger log = LoggerFactory.getLogger(HttpRepositoryImpl.class);

    public String call(String url){
        try {
            URL theUrl = new URL(url);
            BufferedReader in = new BufferedReader(new InputStreamReader(theUrl.openStream()));
            StringBuilder text = new StringBuilder();
            String aux = "";
            while ((aux = in.readLine()) != null) {
                text.append(aux);
            }
            log.debug("Url returns {}", text);
            return text.toString();
        } catch (IOException e) {
            log.error("cannot get content of url", e);
        }
        return null;
    }


    /**
     * посылаем http
     *
     * @param method     - метод http
     * @param httpUrl    - Url, куда посылаем сообщение
     * @param inputParametersString   - параметры запроса
     * @param requestProperies    - параметры RequestProperty
     * @param sslContext - контекст для защищенного соединения, если обычный http, то null
     * @return - массив байтов ответа
     */
    public byte[] sendHttp(String method, String httpUrl, byte[] inputParametersString, Map<String, String> requestProperies, SSLContext sslContext) throws IOException, AutoparkRepositoryException {

        URL url = new URL(httpUrl);

        HttpURLConnection connection;

        if (httpUrl.contains("https") && sslContext != null) {
            SSLSocketFactory sslFactory = sslContext.getSocketFactory();
            log.info("Создали ssl фабрику");
            connection = (HttpsURLConnection) url.openConnection();
            log.info("Открыли соединение");
            ((HttpsURLConnection) connection).setSSLSocketFactory(sslFactory);
            log.info("Подключили ssl фабрику");
        } else {
            connection = (HttpURLConnection) url.openConnection();
        }

        connection.setRequestMethod(method);

        //requestProperies.put("Content-Type", "application/octet-stream");
        if (requestProperies != null) {
            for (Map.Entry<String, String> param : requestProperies.entrySet()) {
                if (StringUtils.isNotEmpty(param.getValue()))
                    connection.setRequestProperty(param.getKey(), param.getValue());
            }
        }

        connection.setDoInput(true);
        connection.setDoOutput(true);
        if ("POST".equalsIgnoreCase(method) || "PUT".equalsIgnoreCase(method)) {
            log.info("Пытаемся послать сообщение");
            postHttpData(connection, inputParametersString);
            return getHttpData(connection);
        } else if ("GET".equalsIgnoreCase(method)) {
            return getHttpData(connection);
        }
        return null;
    }

    /**
     * посылаем post
     * @param conn     - соединение
     * @param inputParametersString - сообщение
     */
    private static void postHttpData(HttpURLConnection conn, byte[] inputParametersString) throws IOException {

        log.info("Собираемся писать ");
        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());

        log.info("Записали ");
        try {
            wr.write(inputParametersString);
            log.info("Отправили ");
        } finally {
            IOUtils.closeQuietly(wr);
        }
    }

    /**
     * посылаем get и получаем ответ
     *
     * @param conn - соединение
     * @throws AutoparkRepositoryException
     */
    private static byte[] getHttpData(HttpURLConnection conn) throws AutoparkRepositoryException {

        byte[] res = null;

        InputStream instrm;
        try {
            log.info("Код ответа http " + conn.getResponseCode());
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                instrm = conn.getInputStream();
            } else {
                instrm = conn.getErrorStream();
            }
        } catch (IOException e) {
            log.error("Не удалось прочитать ответ "+ e);
            throw new AutoparkRepositoryException("Не удалось прочитать ответ", e);
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            try {
                IOUtils.copy(instrm, bos);
                log.info("Прочитали данные ");
            } catch (IOException e) {
                log.error("Не удалось записать ответ "+ e);
                throw new AutoparkRepositoryException("Не удалось записать ответ", e);
            }
            res = bos.toByteArray();
        } finally {
            IOUtils.closeQuietly(bos);
        }


        return res;
    }


    public String callHttpGet(String url) throws AutoparkRepositoryException{
        //String url = "http://www.google.com/search?q=httpClient";

        StringBuffer result = null;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(url);

            // add request header
            //request.addHeader("User-Agent", USER_AGENT);
            HttpResponse response = client.execute(request);

            log.info("Response Code : " + response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException e) {
            log.error("Не удалось сделать запрос " + e);
            throw new AutoparkRepositoryException("Ошибка",e);
        }
        return result.toString();
    }


    public String callHttpPost(String url, Map<String,String> parameters) throws AutoparkRepositoryException{
        //String url = "https://selfsolve.apple.com/wcResults.do";

        StringBuffer result = null;
        try {
            HttpClient client = HttpClientBuilder.create().build();
            HttpPost post = new HttpPost(url);

            // add header
            //post.setHeader("User-Agent", USER_AGENT);

            if(parameters!=null){
                List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
                for(Map.Entry<String,String> entry : parameters.entrySet()){
                    urlParameters.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
                }
                post.setEntity(new UrlEncodedFormEntity(urlParameters));
            }

            HttpResponse response = client.execute(post);
            log.info("Response Code : " + response.getStatusLine().getStatusCode());

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
        } catch (IOException e) {
            log.error("Не удалось сделать запрос " + e);
            throw new AutoparkRepositoryException("Ошибка",e);
        }


        return result.toString();
    }

}
