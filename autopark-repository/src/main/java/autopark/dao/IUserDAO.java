package autopark.dao;


import autopark.domain.User;

import java.util.List;

public interface IUserDAO extends IRootDAO<User> {

    User getByEmail(final String email);

    List<User> getByPhone(final String phone);

    User getByFacebookId(final String facebookId);

    void removeTestUsers(final String phone);

}